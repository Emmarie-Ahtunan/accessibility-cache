---
layout: default
title: Accessibility Cache - Links and Resources
---


# Links and resources

This is by no means an extensive list, but one that should allow you to learn and apply accessible practices no matter your familiarity, skillset, or interest. Start with the list below and be sure to contribute your own.

**Categories**
  
- [Books](#books)
- [Color](#color)
- [Content](#content)
- [Education](#education)
- [Guidelines](#guidelines)
- [Laws](#laws)
- [Live examples](#live-examples)
- [Media](#media)
- [Screen readers](#screen-readers)
- [Testing](#testing)
- [Who to follow](#who-to-follow)
- [Workflow](#workflow)

---

## Books

- [Inclusive Design Patterns](https://shop.smashingmagazine.com/products/inclusive-design-patterns), by Heydon Pickering
- [Inclusive Components](https://www.smashingmagazine.com/printed-books/inclusive-components/), by Heydon Pickering
- [Form Design Patterns](https://shop.smashingmagazine.com/products/form-design-patterns-by-adam-silver), by Adam Silver
- [Color Accessibility Workflows](https://abookapart.com/products/color-accessibility-workflows), by Geri Coady
- [Agile Accessibility Handbook](https://accessibility.deque.com/agile-accessibility-handbook), by Dylan Barrell
- [Accessibility for Everyone](https://abookapart.com/products/accessibility-for-everyone), by Laura Kalbag
- [Accessible Vue](https://accessible-vue.com/), by Marcus Herrmann
- [The Book on Accessibility](https://www.thebookonaccessibility.com/)
- [Just Ask](http://www.uiaccess.com/JustAsk/index.html), by Shawn Lawton Henry

---

## Color

### Contrast

- [Contrast Ratio](https://contrast-ratio.com/)
- [Accessible Colors](https://accessible-colors.com/)
- [Color Review](https://color.review/)
- [whocanuse](https://whocanuse.com/)
- [Accessible Color Generator](https://learnui.design/tools/accessible-color-generator.html)
- [Tanaguru contrast finder](https://contrast-finder.tanaguru.com/)
- [Text on background image a11y check](http://www.brandwood.com/a11y/)
- [Link Contrast Checker](https://webaim.org/resources/linkcontrastchecker/)
- [Contrast Grid](https://contrast-grid.eightshapes.com/)
- [A11y Color Palette](http://a11yrocks.com/colorPalette/)
- [Hex Naw](https://hexnaw.com/)
- [Accessible Brand Colors](https://abc.useallfive.com/)

### Data visualization

- [Viz Palette](https://projects.susielu.com/viz-palette)
- [Data Color Picker](https://learnui.design/tools/data-color-picker.html)
- [ColorBrewer](https://colorbrewer2.org/#type=sequential&scheme=BuGn&n=3)
- [Inclusive Color Sequences for Data Viz in 6 Steps](https://medium.com/design-ibm/inclusive-color-sequences-for-data-viz-in-6-steps-712869b910c2), by Voranouth Supadulya and Cameron Calder
- [How to Use Color Blind Friendly Palettes to Make Your Charts Accessible](https://venngage.com/blog/color-blind-friendly-palette/), by Rachel Cravit 
- [A detailed guide to colors in data vis style guides](https://blog.datawrapper.de/colors-for-data-vis-style-guides/), by Lisa Charlotte Muth

### Palettes

- [Accessible color palette builder](https://toolness.github.io/accessible-color-matrix/)
- [ColorBox](https://colorbox.io/)
- [Color Safe](http://colorsafe.co/)
- [Geenes](https://geenes.app/)

### Color blindness

- [Colorblind Web Page Filter](https://www.toptal.com/designers/colorfilter)
- [Sim Daltonism](https://michelf.ca/projects/sim-daltonism/)
- [Colblindor](https://www.color-blindness.com/coblis-color-blindness-simulator/)

### Theory and practice

- [Color is Relative](http://colorisrelative.com/color/)
- [Designing accessible color systems](https://stripe.com/blog/accessible-color-systems), by Daryl Koopersmith and Wilson Miner

---

## Content

- [Hemingway Editor](http://www.hemingwayapp.com/)
- [Readability Analyzer](https://datayze.com/readability-analyzer.php)

---

## Education

### Courses - Paid

- [Deque University](https://dequeuniversity.com/)
- [Access University](https://www.levelaccess.com/solutions/training/access-university/)
- [The Paciello Group ARC TPG Tutor](https://www.paciellogroup.com/products/tpg-tutor/)
- [Udemy Accessibility Courses](https://www.udemy.com/topic/web-accessibility/)
- [WebAIM Web Accessibility Training](https://webaim.org/training/)
- [FrontendMasters - ARIA](https://frontendmasters.com/courses/web-accessibility/aria/), by Jon Kuperman

### Courses - Free

- [UX Foundations: Accessibility](https://www.linkedin.com/learning/ux-foundations-accessibility/welcome?u=2255073), by Derek Featherstone
- [Accessibility for Web Design](https://www.linkedin.com/learning/accessibility-for-web-design/welcome?u=2255073), by Derek Featherstone
- [Web Accessibility](https://www.udacity.com/course/web-accessibility--ud891), by Google
- [Start Building Accessible Web Applications Today](https://egghead.io/courses/start-building-accessible-web-applications-today), by Marcy Sutton
- [Introduction to Web Accessibility](https://www.edx.org/course/web-accessibility-introduction), by edX
- [Accessibility Fundamentals for the Web](https://siteimprove.litmos.com/self-signup/register/1039634?type=1), by Siteimprove
- [Accessibility Training & Workshops](https://it.rutgers.edu/it-accessibility-initiative/knowledgebase/accessibility-training-workshops/), by Rutgers
- [An Introduction to Accessibility and Inclusive Design](https://www.coursera.org/learn/accessibility)

### Talks and series

- [A11ycasts](https://www.youtube.com/playlist?list=PLNYkxOF6rcICWx0C9LVWWVqvHlYJyqw7g), with Rob Dodson
- [Debugging Accessibility with Alice Boxhall](https://www.youtube.com/watch?v=B9qzdVcIj5U&feature=youtu.be)
- [Marcy Sutton Talks](https://marcysutton.com/talks/)

### Guides

- [Accessibility for Teams](https://accessibility.digital.gov/)
- [Teach Access](https://teachaccess.github.io/)
- [Accessibility Developer Guide](https://www.accessibility-developer-guide.com/)
- [Level Access Accessible Web Demo wiki](https://labs.levelaccess.com/index.php/Main_Page)
- [The A11Y Project](https://www.a11yproject.com/)
- [MDN Web Docs - Accessibility](https://developer.mozilla.org/en-US/docs/Web/Accessibility), by Mozilla
- [Fable's Assistive Technology Glossary](https://makeitfable.com/glossary/)
- [Access Guide](https://www.accessguide.io/)
- [How to write accessibility acceptance criteria](https://bbc.github.io/accessibility-news-and-you/guides/accessibility-acceptance-criteria.html)

---

## Guidelines

- [Web Content Accessibility Guidelines (WCAG) 2.1](https://www.w3.org/TR/WCAG21/)
- [W3C Accessibility Guidelines (WCAG) 3.0 (Working Draft)](https://www.w3.org/TR/wcag-3.0/)
- [Authoring Tool Accessibility Guidelines (ATAG) 2.0](https://www.w3.org/TR/ATAG20/)
- [WAI-ARIA Authoring Practices 1.1](https://www.w3.org/TR/wai-aria-practices-1.1/)
- [WCAG Evaluation Methodology](https://www.w3.org/TR/WCAG-EM/)

---

## Laws

### United States

- [Section 508](https://www.section508.gov/manage/laws-and-policies)
- [ADA Standards for Accessible Design](https://www.ada.gov/2010ADAstandards_index.htm)
- [21st Century Communications and Video Accessibility Act (CVAA)](https://www.fcc.gov/consumers/guides/21st-century-communications-and-video-accessibility-act-cvaa)

### Canada

- [Accessibility for Ontarians with Disabilities Act (AODA)](https://www.aoda.ca/)

### Europe

- [Accessibility requirements suitable for public procurement of ICT products and services in Europe](http://mandate376.standards.eu/standard) - EU
- [Equality Act 2010](https://www.legislation.gov.uk/ukpga/2010/15/contents) - UK
- [Référentiel général d'amélioration de l'accessibilité – RGAA Version 4](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/) - France
- [Barrierefreie-Informationstechnik-Verordnung (BITV 2) ](http://www.gesetze-im-internet.de/bitv_2_0/index.html) - Germany
- [Disability Act 2005](http://www.irishstatutebook.ie/eli/2005/act/14/enacted/en/html) - Ireland

### Other

- [World Wide Web Access: Disability Discrimination Act Advisory Notes ver 4.1 (2014)](https://humanrights.gov.au/our-work/disability-rights/world-wide-web-access-disability-discrimination-act-advisory-notes-ver) - Australia
- [Guidelines for Indian Government Websites](https://web.guidelines.gov.in/) - India
- [NZ Government Web Standards](https://www.digital.govt.nz/standards-and-guidance/nz-government-web-standards/) - New Zealand
- [Web Accessibility Laws & Policies](https://www.w3.org/WAI/policies/) - Global list of governemtal policies

---

## Live examples

- [Inclusive Components](https://inclusive-components.design/)
- [No Style Design System](https://nostyle.herokuapp.com/)
- [eBay MIND Patterns](https://ebay.gitbook.io/mindpatterns/)
- [A11Y Style Guide](https://a11y-style-guide.com/style-guide/)
- [Axess Lab Practice Examples](https://axesslab.com/practical-accessibility-improvements/)
- [Accessible Components](https://github.com/scottaohara/accessible_components)
- [Accessible UI Component Libraries Roundup](https://www.digitala11y.com/accessible-ui-component-libraries-roundup/)

---

## Media

- [Descript](https://www.descript.com/)

---

## Screen readers

- [Screen Reader Keyboard Shortcuts and Gestures](https://dequeuniversity.com/screenreaders/)

---

## Testing

### Companies

- [WeCo](https://theweco.com/)
- [Fable](https://makeitfable.com/)
- [Tenon](https://tenon.io/)

### Tools

- [WAVE](https://wave.webaim.org/extension/)
- [axe](https://www.deque.com/axe/)
- [HTML_CodeSniffer](https://squizlabs.github.io/HTML_CodeSniffer/)
- [tota11y](https://khan.github.io/tota11y/)
- [WCAG-EM Report Tool](https://www.w3.org/WAI/eval/report-tool/#!/)
- [SortSite](https://www.powermapper.com/products/sortsite/)
- [The Universal Score](https://universalscore.global/)
- [Accessibility project WCAG checklist](https://www.a11yproject.com/checklist/)
- [AccessLint](https://accesslint.com/)
- [W3C Web Accessibility Tools list](http://www.w3.org/WAI/ER/tools/)

### Audits and evaluation

- [Accessibility Testing](https://racheleditullio.com/projects/accessibility-testing/), Rachel Tullio
- [Accessibility monitoring: How we test](https://www.gov.uk/guidance/accessibility-monitoring-how-we-test), GOV.UK
- [A Web Accessibility Assessment Hands-on Tutorial](https://dequeuniversity.com/videos/web/testing/case-study), Deque

---

## Who to follow

- [Derek Featherstone](https://twitter.com/feather)
- [Marcy Sutton](https://twitter.com/marcysutton)
- [Heydon Pickering](https://twitter.com/heydonworks)
- [Léonie Watson](https://twitter.com/LeonieWatson)
- [Adam Silver](https://twitter.com/adambsilver)
- [Rob Dodson](https://twitter.com/rob_dodson)
- [Alice Boxhall](https://twitter.com/sundress)
- [Charles Hall](https://twitter.com/atCharlesHall)
- [Claudio Luís Vera](https://twitter.com/modulist)
- [Marcelo Paiva](https://twitter.com/muqueca)
- [Matt May](https://twitter.com/mattmay)
- [Patrick Lauke](https://twitter.com/patrick_h_lauke)
- [Paciello Group](https://twitter.com/paciellogroup)

---

## Workflow

- [A Toolkit for Digital Accessibility Requirements](http://bit.ly/accessibility-toolkit)
- [Accessibility Bluelines annotation tools for content and behavior](https://www.figma.com/community/file/779827094223635810/Accessibility-bluelines)
